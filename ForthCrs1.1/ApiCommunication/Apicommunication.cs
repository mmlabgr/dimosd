﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ForthCrs1._1.OpenSea;
using System.Threading.Tasks;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using QuickType;
using Newtonsoft.Json;
using ForthCrs1._1.Dialogs;
using Microsoft.Bot.Builder.Dialogs;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

namespace ForthCrs1._1.ApiCommunication
{
    public class Apicommunication
    {
        const string uriBase = "https://api.cognitive.microsoft.com/bing/v7.0/images/search";
        //Initialize a new ForthcrsClient
        public static IFcrsFerriesWSservice client = new IFcrsFerriesWSservice();
        public static string imageArrivalUrl;
        const string accessKey = "";
      
        //Loading ForthCrs Headers
        public static THeaderInfo HeadersLoad()
        {

            THeaderInfo headers = new THeaderInfo();
            headers.AgentCode = "";
            headers.AgentUser = "";
            headers.AgentPasswd = "";
            headers.AgentSignature = "";
            var myheaders = headers;
            return myheaders;

        }
   
        //Calling the Api to get the Arrival-Departure combination
        public static List<From> LocationCombination()
        {

            THeaderInfo head = Apicommunication.HeadersLoad();

            //Calling the api
            string locationCombination = client.GetLocationsCombinations(head, "");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(locationCombination);
            string jsonText = JsonConvert.SerializeXmlNode(doc);
            //Deserealize the returning xml to an object
            var root = Welcome.FromJson(jsonText);
            var departures = root.Departures;
            var from = root.Departures.From;
            return from;

        }

        //Creating a Ttimetable object to save user properties
        public static TTimetableReq ClientProperties()
        {
  
            THeaderInfo myheaders = Apicommunication.HeadersLoad();
            TTimetableReq clientproperties = new TTimetableReq();
            return clientproperties;

        }

        //We call the Api with the Ttimetable with the user properties and we get the itinerariesTable
        public static List<TTimetableAns1Ext> GetItinerariesExt(TTimetableReq timetable)
        {
            THeaderInfo myheaders = Apicommunication.HeadersLoad();
            var RespondTable = client.GetItinerariesExt(myheaders, timetable);
            var itinerariesTable = RespondTable.ToList();
            return itinerariesTable;

        }

        //We call the Api to get the list with vehicles
        public static List<TVehiclesListStruct> GetVehicleList()
        {
            THeaderInfo myhead = Apicommunication.HeadersLoad();
            var vehicle = client.GetVehicleTypesList(myhead);
            var vehicleType = vehicle.ToList();
            return vehicleType;

        }

        //We call the Api with and we get the Itinerary pricing answer for all passengers and vehicles
        public static ItineraryPricingAnswer[] PriceRequest(List<TMsgPricingPassReq> PpP, List<TMsgPricingVehicleReq> PpV, List<TClassAnalysisLocal> CaL, IDialogContext context)
        {


            ItineraryPricingRequest priceRequest = new ItineraryPricingRequest();
            List<ItineraryPricingRequest[]> pr = new List<ItineraryPricingRequest[]>();
            THeaderInfo headd = Apicommunication.HeadersLoad();

            string company = null;
            string departure = null;
            string arrival = null;
            string departureTime = null;
            string vesesselid = null;
            string departureDate = null;
            string arrivalLoc = null;
            context.PrivateConversationData.TryGetValue<string>("ArrivalLocation", out arrivalLoc);
            context.PrivateConversationData.TryGetValue<string>("Company", out company);
            context.PrivateConversationData.TryGetValue<string>("DepartureLocationCode", out departure);
            context.PrivateConversationData.TryGetValue<string>("ArrivalLocationCode", out arrival);
            context.PrivateConversationData.TryGetValue<string>("DepartureTime", out departureTime);
            context.PrivateConversationData.TryGetValue<string>("VesselId", out vesesselid);
            context.PrivateConversationData.TryGetValue<string>("DepartureDate", out departureDate);
            priceRequest.ArrStation = arrival;
            priceRequest.Company = company;
            priceRequest.DepDate = departureDate;
            priceRequest.Deptime = departureTime;
            priceRequest.DepStation = departure;
            priceRequest.PricingPerPassenger = Traveldialog1.AllPassTable.ToArray();
            priceRequest.PricingPerVehicle = Traveldialog1.AllVehTable.ToArray();
            priceRequest.VesselID = vesesselid;

            //call Bing image search Api to get an image of the arrival location
            string imagePort = arrivalLoc + " port";
            var uriQuery = uriBase + "?q=" + Uri.EscapeDataString(imagePort);
            WebRequest request = HttpWebRequest.Create(uriQuery);
            request.Headers["Ocp-Apim-Subscription-Key"] = accessKey;
            HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result;
            string json = new StreamReader(response.GetResponseStream()).ReadToEnd();
            var arrivalLocImage = JObject.Parse(json);
            var root = arrivalLocImage["value"][0]["contentUrl"].ToString();
            //Saving the image Url
            imageArrivalUrl = root;

            ItineraryPricingRequest[] completeprice = new ItineraryPricingRequest[] { priceRequest };
            var completePriceAnswer = client.ItineraryPricing(headd, completeprice);
            return completePriceAnswer;

        }
        public static TIssueAnswer[] Reservationanswer(List<TClassAnalysisLocal> classanalysssis, List<TReserveIssuePerPassengerLocal> reserveperpass, List<TReserveIssuePerVehicleLocal> reserverveh, IDialogContext context)
        {

            TReserveIssueRequest reserveRequest = new TReserveIssueRequest();
            List<TReserveIssueRequest[]> rir = new List<TReserveIssueRequest[]>();
            THeaderInfo headd = Apicommunication.HeadersLoad();

            string company = null;
            string departure = null;
            string arrival = null;
            string departureTime = null;
            string vesesselid = null;
            string departureDate = null;
            string arrivalLoc = null;
            context.PrivateConversationData.TryGetValue<string>("ArrivalLocation", out arrivalLoc);
            context.PrivateConversationData.TryGetValue<string>("Company", out company);
            context.PrivateConversationData.TryGetValue<string>("DepartureLocationCode", out departure);
            context.PrivateConversationData.TryGetValue<string>("ArrivalLocationCode", out arrival);
            context.PrivateConversationData.TryGetValue<string>("DepartureTime", out departureTime);
            context.PrivateConversationData.TryGetValue<string>("VesselId", out vesesselid);
            context.PrivateConversationData.TryGetValue<string>("DepartureDate", out departureDate);

            reserveRequest.DepDate = departureDate;
            reserveRequest.Deptime = departureTime;
            reserveRequest.DepStation = departure;
            reserveRequest.ArrStation = arrival;
            reserveRequest.Company = company;
            reserveRequest.VesselID = vesesselid;

            reserveRequest.ClassAnalysis = classanalysssis.ToArray();
            reserveRequest.ReserveIssuePerPassenger = reserveperpass.ToArray();
            reserveRequest.ReserveIssuePerVehicle = reserverveh.ToArray();
            TReserveIssueRequest[] completeReserveRequest = new TReserveIssueRequest[] { reserveRequest };
            var completeReserveAnswer = client.IssuePrepayedTickets(headd, completeReserveRequest);
<<<<<<< HEAD

            

=======
<<<<<<< HEAD

            

=======
            
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
            return completeReserveAnswer;
        }
        
        public static TCancelPNRAnswer CancelReservation(string reservationCode)

<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
        public static TCancelPNRAnswer CancelReservation(string reservationCode)

>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
        {
            THeaderInfo headd = Apicommunication.HeadersLoad();
            TCancelPNRRequest cancelrequest = new TCancelPNRRequest();
            cancelrequest.CRSReservationCode = reservationCode;
            var cancelAnswer = client.CancelPNR(headd, cancelrequest);
            return cancelAnswer;

        }
    }

}