﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var welcome = Welcome.FromJson(jsonString);

namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Welcome
    {
        [JsonProperty("departures")]
        public Departures Departures { get; set; }
    }

    public partial class Departures
    {
        [JsonProperty("from")]
        public List<From> From { get; set; }
    }

    public partial class From
    {
        [JsonProperty("@code")]
        public string Code { get; set; }

        [JsonProperty("@name")]
        public string Name { get; set; }

        [JsonProperty("to")]
        public ToUnion To { get; set; }
    }

    public partial class ToElement
    {
        [JsonProperty("@code")]
        public string Code { get; set; }

        [JsonProperty("@name")]
        public string Name { get; set; }
    }

    public partial struct ToUnion
    {
        public ToElement ToElement;
        public List<ToElement> ToElementArray;
    }

    public partial class Welcome
    {
        public static Welcome FromJson(string json) => JsonConvert.DeserializeObject<Welcome>(json, QuickType.Converter.Settings);
    }

    public partial struct ToUnion
    {
        public ToUnion(JsonReader reader, JsonSerializer serializer)
        {
            ToElement = null;
            ToElementArray = null;

            switch (reader.TokenType)
            {
                case JsonToken.StartArray:
                    ToElementArray = serializer.Deserialize<List<ToElement>>(reader);
                    return;
                case JsonToken.StartObject:
                    ToElement = serializer.Deserialize<ToElement>(reader);
                    return;
            }
            throw new Exception("Cannot convert ToUnion");
        }

        public void WriteJson(JsonWriter writer, JsonSerializer serializer)
        {
            if (ToElement != null)
            {
                serializer.Serialize(writer, ToElement);
                return;
            }
            if (ToElementArray != null)
            {
                serializer.Serialize(writer, ToElementArray);
                return;
            }
            throw new Exception("Union must not be null");
        }
    }

    public static class Serialize
    {
        public static string ToJson(this Welcome self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
    }

    internal class Converter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ToUnion) || t == typeof(ToUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (t == typeof(ToUnion) || t == typeof(ToUnion?))
                return new ToUnion(reader, serializer);
            throw new Exception("Unknown type");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var t = value.GetType();
            if (t == typeof(ToUnion))
            {
                ((ToUnion)value).WriteJson(writer, serializer);
                return;
            }
            throw new Exception("Unknown type");
        }

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new Converter(),
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
