﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ForthCrs1._1.Cards
{[Serializable]
    public class CarouselPricePrecentation : IDialog<object>
    {

        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {

                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }
        private string firstnameUser;
        private List<int> pricesppass;
     //   private List<string> ticketsNo;
        private List<int> pricespveh;
        private List<string> passname;
        private List<string> passinitial;
        private List<string> vehTickets;
        private int travsnumber;
        private int vehNumber;
        private List<string> vehtype;
    //    public static List<Attachment> travitem = new List<Attachment>();
       public static List<Attachment> alltravitem;
   
        public async Task StartAsync(IDialogContext context)
        {



            await MessageReceivedAsync(context);
            //  context.Wait(this.MessageReceivedAsync);

        }
        public async virtual Task MessageReceivedAsync(IDialogContext context)
        {
            var welcomeMessage = context.MakeMessage();
            welcomeMessage.Text = "Your Invoice Details below";
            string firstname = null;
            await context.PostAsync(welcomeMessage);
            context.PrivateConversationData.TryGetValue("FirstName", out firstname);
            context.PrivateConversationData.TryGetValue<List<string>>("PassengersName", out passname);
            context.PrivateConversationData.TryGetValue<int>("TravellersNumber", out travsnumber);
            context.PrivateConversationData.TryGetValue<List<int>>("PricePerPass", out pricesppass);
        //    context.PrivateConversationData.TryGetValue<List<string>>("PassengerTickets", out ticketsNo);
            context.PrivateConversationData.TryGetValue<List<string>>("PassengersInitial", out passinitial);
            context.PrivateConversationData.TryGetValue<int>("VehiclesNumber", out vehNumber);
            //context.PrivateConversationData.TryGetValue<List<string>>("VehiclesTickets", out vehTickets);
            context.PrivateConversationData.TryGetValue<List<int>>("PricePerVehicle", out pricespveh);
            context.PrivateConversationData.TryGetValue<List<string>>("VehicleType", out vehtype);


            var passengersname = passname.ToArray();
         //   var tickNumb = ticketsNo.ToArray();
           List<Attachment> receiptItems = new List<Attachment>();
            string firstnameUser = UppercaseFirst(firstname);
            for (int k = 1; k <= travsnumber;)
            {
                var receipt = GetProfileHeroCard(title: passinitial[k - 1] + "." + passengersname[k - 1], subtitle:"Ticket Price :" , text: (pricesppass[k - 1] * 0.01).ToString() + " Euro", cardimage:new CardImage(url: "https://cdn4.iconfinder.com/data/icons/dot/128/taxi_passanger.png"));
                receiptItems.Add(receipt);
                
                k++;
            }
            for (int l = 1; l <= vehNumber;)
            {
                var receipt = GetProfileHeroCard(title: vehtype[l - 1], subtitle:"Ticket Price :" , text :(pricespveh[l - 1] * 0.01).ToString() + " Euro", cardimage: new CardImage(url: "https://cdn4.iconfinder.com/data/icons/travel-gang/512/Travel_11-128.png") );
                receiptItems.Add(receipt);
               
                l++;
            }
            alltravitem = receiptItems;
            await this.DisplayHeroCard(context);
        

       
        }

        public async Task DisplayHeroCard(IDialogContext context)
        {



            var reply = context.MakeMessage();
           
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;

            var allitemtrav = alltravitem;
            reply.Attachments = allitemtrav;
            
            await context.PostAsync(reply);

            alltravitem.Clear();

       
            context.Done(reply);

        }
        private static Attachment GetProfileHeroCard(string title,string subtitle,string text ,CardImage cardimage)
        {

            var heroCard = new HeroCard
            {
                // title of the card  
                Title = title,
                //Detail Text  
                Subtitle = subtitle,

              Text = text ,

                
                Images = new List<CardImage>() { cardimage },                

            };

            return heroCard.ToAttachment();

        }



    }
}