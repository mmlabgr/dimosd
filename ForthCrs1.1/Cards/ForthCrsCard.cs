﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ForthCrs1._1.Cards
{
    [Serializable]
    public class ForthCrsCard : IDialog<object>

    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(this.MessageReceivedAsync);
        }
        public async virtual Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            await this.DisplayHeroCard(context);

        }
 
        public async Task DisplayHeroCard(IDialogContext context)
        {

            var replyMessage = context.MakeMessage();
            Attachment attachment = GetProfileHeroCard(); ;
            replyMessage.Attachments = new List<Attachment> { attachment };
            await context.PostAsync(replyMessage);
            context.Done(replyMessage);
            
        }
        private static Attachment GetProfileHeroCard()
        {
            var heroCard = new HeroCard
            {
                // title of the card  
                Title = "FORTHcrs",
                //subtitle of the card  
                Subtitle = "Ticket Reservation System",
                // navigate to page , while tab on card  
                Tap = new CardAction(ActionTypes.OpenUrl, "Learn More", value: "http://www.forthcrs.com/"),
                //Detail Text  
                Text = "SeaOnLine™ is the most widely used reservation system in the Ferry industry, covering the maximum complexity and fulfilling all requirements of the Ferry Operator",
                // list of  Large Image  
                Images = new List<CardImage> { new CardImage("http://2015.digi.travel/wp-content/uploads/2015/10/Forth-crs_logo-e1445374062393.png") },
                // list of buttons   
                Buttons = new List<CardAction> { new CardAction(ActionTypes.OpenUrl, "Visit our web page", value: "https://www.forthcrs.com/"), new CardAction(ActionTypes.PostBack, "Continue with our bot" ,value:"Let's continue our conversation !")}
                          
               
             };
            
            return heroCard.ToAttachment();

        }

    }

}