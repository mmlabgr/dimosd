﻿using ForthCrs1._1.Dialogs;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ForthCrs1._1.Cards
{
    [Serializable]
    public class PricePresentationCard : IDialog<object>

    {
        private static string text1;
        public async Task StartAsync(IDialogContext context)
        {
            await this.MessageReceivedAsync(context);
        }
        public async virtual Task MessageReceivedAsync(IDialogContext context)
        {
            
            await this.DisplayThumbnailCard(context);

        }

        public async Task DisplayThumbnailCard(IDialogContext context)
        {
            int vehNumb = 0;
            int travelNumb = 0;
            List<int> pricesppass = null;
            List<int> pricespveh = null;

            var replyMessage = context.MakeMessage();

            context.PrivateConversationData.TryGetValue<List<int>>("PricePerVehicle", out pricespveh);
            context.PrivateConversationData.TryGetValue<List<int>>("PricePerPass", out pricesppass);
            context.PrivateConversationData.TryGetValue<int>("VehiclesNumber", out vehNumb);
            context.PrivateConversationData.TryGetValue<int>("TravellersNumber", out travelNumb);
            var sb = new System.Text.StringBuilder();
            for (int k = 1; k <= travelNumb; k++)
            {

                sb.Append("The amount for passenger " + k.ToString() + " is : " + (pricesppass[k - 1] * 0.01).ToString() + " Euro.   " + "\n\n");

            }

            for (int m = 1; m <= vehNumb; m++)
            {

                sb.Append("The amount for vehicle " + m.ToString() + " is: " + (pricespveh[m - 1] * 0.01).ToString() + " Euro.    " + "\n\n");

            }
            sb.Append("Total amount of your reservation with extra fees is " + (Traveldialog1.completeAmount * 0.01).ToString() + " Euro.   ");
            text1 = sb.ToString();
            
            Attachment attachment = GetProfileThumbnailCard();


            replyMessage.Attachments = new List<Attachment> { attachment };
            await context.PostAsync(replyMessage);
            context.Done(replyMessage);

        }
        private static Attachment GetProfileThumbnailCard()
        {
            var thumbnailCard = new ThumbnailCard
            {
                // title of the card  
                Title = "FORTHcrs",
                //subtitle of the card  
                Subtitle = "Here are the prices for the tickets you ordered",

                // navigate to page , while tab on card  
                Tap = new CardAction(ActionTypes.OpenUrl, value: "http://www.forthcrs.com/"),
                //Detail Text  
                Text = text1,
                // list of  Large Image  
                Images = new List<CardImage> {new CardImage ("https://s.nbst.gr/files/1/2013/02/05/forthcrs.medium.jpg") }
              

            };

            return thumbnailCard.ToAttachment();

        }

    }

}