﻿using ForthCrs1._1.Dialogs;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace ForthCrs1._1.Cards
{
    [Serializable]
    public class ReceiptCardDialog : IDialog<object>
    {
        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {

                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }
        private string firstnameUser;
        private List<int> pricesppass;
        private List<string> ticketsNo;
        private List<int> pricespveh;
        private List<string> passname;
        private List<string> passinitial;
        private List<string> vehTickets;
        private int travsnumber;
        private int vehNumber;
        private List<string> vehtype;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
      //  public static List<ReceiptItem> receiptItem = new List<ReceiptItem>();
        public static List<ReceiptItem> allItems;
        private static List<string> extraFee;
        private static int extracharge;
<<<<<<< HEAD
=======
=======
        public static List<ReceiptItem> receiptItem = new List<ReceiptItem>();
        public static List<ReceiptItem> allItems = receiptItem;

>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
        public async Task StartAsync(IDialogContext context)
        {
            await this.MessageReceivedAsync(context);
        }
        public async virtual Task MessageReceivedAsync(IDialogContext context)
        {
            
            var welcomeMessage = context.MakeMessage();
            welcomeMessage.Text = "Your Invoice Details below ,click Request Email to send you Email with your ForthCrs code so you can cancel your reservation at any time";
            string firstname = null;
            await context.PostAsync(welcomeMessage);
            context.PrivateConversationData.TryGetValue("FirstName", out firstname);
            context.PrivateConversationData.TryGetValue<List<string>>("PassengersName", out passname);
            context.PrivateConversationData.TryGetValue<int>("TravellersNumber", out travsnumber);
            context.PrivateConversationData.TryGetValue<List<int>>("PricePerPass", out pricesppass);
            context.PrivateConversationData.TryGetValue<List<string>>("PassengerTickets", out ticketsNo);
            context.PrivateConversationData.TryGetValue<List<string>>("PassengersInitial", out passinitial);
            context.PrivateConversationData.TryGetValue<int>("VehiclesNumber", out vehNumber);
            context.PrivateConversationData.TryGetValue<List<string>>("VehiclesTickets", out vehTickets);
            context.PrivateConversationData.TryGetValue<List<int>>("PricePerVehicle", out pricespveh);
            context.PrivateConversationData.TryGetValue<List<string>>("VehicleType", out vehtype);
<<<<<<< HEAD
            context.PrivateConversationData.TryGetValue<List<string>>("ExtraCharges",out extraFee);
            extracharge = Int32.Parse(extraFee[0]);

=======
<<<<<<< HEAD
            context.PrivateConversationData.TryGetValue<List<string>>("ExtraCharges",out extraFee);
            extracharge = Int32.Parse(extraFee[0]);

=======
          
            
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
            var passengersname = passname.ToArray();
            var tickNumb = ticketsNo.ToArray();
            List<ReceiptItem> receiptItems = new List<ReceiptItem>();
            string firstnameUser = UppercaseFirst(firstname);
            for(int k=1; k<= travsnumber;)
            {
                ReceiptItem rerceipt = new ReceiptItem(title: passinitial[k - 1] + "." + passengersname[k - 1], subtitle:"TicketNo: "+ticketsNo[k - 1], price: (pricesppass[k - 1]*0.01).ToString()+" Euro",quantity:"1",image:new CardImage(url: "https://cdn4.iconfinder.com/data/icons/dot/128/taxi_passanger.png"));
                receiptItems.Add(rerceipt);
              //  receiptItem.AddRange(receiptItems);
                k++;
            }
            for (int l =1;l<= vehNumber;)
            {
                ReceiptItem rerceipt = new ReceiptItem(title:vehtype[l-1] , subtitle: "TicketNo : " + vehTickets[l - 1], price: (pricespveh[l - 1] * 0.01).ToString() + " Euro",quantity:"1", image: new CardImage(url: "https://cdn4.iconfinder.com/data/icons/travel-gang/512/Travel_11-128.png"));
                receiptItems.Add(rerceipt);
           //     receiptItem.AddRange(receiptItems);
                l++;
            }
            allItems = receiptItems;
            await this.DisplayReceiptCard(context);
        }
     
        public async Task DisplayReceiptCard(IDialogContext context)
        {

            var replyMessage = context.MakeMessage();
            Attachment attachment = getBotReceiptCard(); ;
            replyMessage.Attachments = new List<Attachment> { attachment };
            await context.PostAsync(replyMessage);
            allItems.Clear();
<<<<<<< HEAD
          //  receiptItem.Clear();
=======
<<<<<<< HEAD
          //  receiptItem.Clear();
=======
            receiptItem.Clear();
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
            context.Done(replyMessage);
        }
        private static Attachment getBotReceiptCard()
        {

            var receiptCard = new ReceiptCard
            {
                Title = "Online Tickets Reservation",
           

                Items = allItems,

                Facts = null,

                Tax = (extracharge*0.01).ToString()+ " Euro",
                
                Tap = null,

                Vat = null,
                
                Total = (Traveldialog1.completeAmount*0.01).ToString()+ " Euro",
               
                Buttons = new List<CardAction>
  {
     new CardAction(ActionTypes.PostBack,"Request email",value:"Email Request")

  }
            };
           
            return receiptCard.ToAttachment();
        }
       
    }

    

}