﻿using ForthCrs1._1.Dialogs;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ForthCrs1._1.Cards
{
    [Serializable]
    public class ReservationCard : IDialog<object>

    {

        public async  Task StartAsync(IDialogContext context)
        {



            await MessageReceivedAsync(context);
          //  context.Wait(this.MessageReceivedAsync);

        }
        public async virtual Task MessageReceivedAsync(IDialogContext context)
        {
            
            var welcomeMessage = context.MakeMessage();
            string arrivalloc = null;
            context.PrivateConversationData.TryGetValue<string>("ArrivalLocation", out arrivalloc);
            welcomeMessage.Text = "You will ship to " + UppercaseFirst(arrivalloc);

            await context.PostAsync(welcomeMessage);
            await this.DisplayHeroCard(context);

        }

        public async Task DisplayHeroCard(IDialogContext context)
        {

            var replyMessage = context.MakeMessage();
            Attachment attachment = GetProfileHeroCard(); ;
            replyMessage.Attachments = new List<Attachment> { attachment };
            await context.PostAsync(replyMessage);
            context.Done(replyMessage);

        }
        private static Attachment GetProfileHeroCard()
        {

            var heroCard = new HeroCard
            {
                // title of the card  
                Title = "Reserve your tickets",
                //Detail Text  
                Subtitle = "The total ammount of your reservation is :",

                Text = (Traveldialog1.completeAmount * 0.01).ToString() + " Euro",
                // list of  Large Image  
                Images = new List<CardImage> { new CardImage(ApiCommunication.Apicommunication.imageArrivalUrl) },
                // list of buttons   
                Buttons = new List<CardAction> { new CardAction(ActionTypes.PostBack, "Buy Tickets", value: "BuyTickets") }

            };

            return heroCard.ToAttachment();

        }
        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {

                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }
    }


}