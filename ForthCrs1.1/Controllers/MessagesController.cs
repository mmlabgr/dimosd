﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using ForthCrs1._1.ProfileForm;
using ForthCrs1._1.Dialogs;
using ForthCrs1._1.Cards;

namespace ForthCrs1._1
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            

            if (activity.Type == ActivityTypes.Message)
            {
           
                //Fisrt message of the user
                await Conversation.SendAsync(activity, () => new RootDialog());
<<<<<<< HEAD
                         
=======
<<<<<<< HEAD
                         
=======
                
          
             
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
            }
            else
            {

                HandleSystemMessage(activity);

            }
            //Return Response
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }



        private Activity HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
<<<<<<< HEAD
              /*  StateClient client = message.GetStateClient();
=======
<<<<<<< HEAD
              /*  StateClient client = message.GetStateClient();
=======
                StateClient client = message.GetStateClient();
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
                BotData userData = client.BotState.GetPrivateConversationData(message.ChannelId, message.Conversation.Id, message.From.Id);
                userData.SetProperty<string>("UserName", "");
                userData.SetProperty<bool>("AskedForUserName", false);
                client.BotState.SetPrivateConversationData(message.ChannelId, message.Conversation.Id, message.From.Id, userData);
                ConnectorClient connector = new ConnectorClient(new Uri(message.ServiceUrl));
                Activity replyMessage = message.CreateReply("Personal data has been deleted.");
                return replyMessage;*/

            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }
    }
}