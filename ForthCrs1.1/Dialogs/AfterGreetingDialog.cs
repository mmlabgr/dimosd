﻿using ForthCrs1._1.ProfileForm;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ForthCrs1._1.Dialogs
{
    [Serializable]
    public class AfterGreetingDialog : IDialog<ProfileForma>
    {
        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }

        public async Task StartAsync(IDialogContext context)
        {
            // Root dialog initiates and waits for the next message from the user. 
            // When a message arrives, call MessageReceivedAsync.
            string name = String.Empty;
            context.PrivateConversationData.TryGetValue<string>("FirstName", out name);
            await context.PostAsync(String.Format("Goodbye {0} , I hope to see you back soon !!!", UppercaseFirst(name)));
            context.Done(name);
        }

    }

}