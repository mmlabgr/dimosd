﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;

namespace ForthCrs1._1.Dialogs
{
    [Serializable]
    public class CaseNoRoots
    {



        //they hold the data gathering from the form

        [Prompt("Sorry for the incovinience,try another departure date ? (MM/DD/YYYY){||}")]
        public DateTime DepartureTime;


        public static IForm<CaseNoRoots> BuildForm()
        {
            return new FormBuilder<CaseNoRoots>()


                .OnCompletion(async (context, TravelDialog2) =>
                {//Set Bot UserData

                    context.PrivateConversationData.SetValue<DateTime>("DepartureTime", TravelDialog2.DepartureTime);

                })
                .Build();

        }

    }

}