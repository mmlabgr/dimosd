﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;

namespace ForthCrs1._1.Dialogs
{
    [Serializable]

    public class DepartureDate
    {

        [Prompt("When do you want to ship ? (MM/dd/YYYY){||}")]
        public DateTime Departuredate;

        [Prompt("I would like to know the number of passengers?(1-9 Travellers){||}")]
        [Numeric(1, 9)]
        public int TravellersNumber;

        public static IForm<DepartureDate> BuildForm()

        {
            return new FormBuilder<DepartureDate>()
                 .Message("Let me know some details about your shipment")
                 .OnCompletion(async (context, travelDialog) =>
                 {//Set Bot UserData

                    context.PrivateConversationData.SetValue<DateTime>("DepartureDate",travelDialog.Departuredate);
                    context.PrivateConversationData.SetValue<int>("TravellersNumber", travelDialog.TravellersNumber);
                     await context.PostAsync("Thank you!!");
                    //tell the user tha the form is complete
                    


                })
                .Build();
            }


    }

} 