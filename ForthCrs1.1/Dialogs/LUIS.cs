﻿using ForthCrs1._1.ProfileForm;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
<<<<<<< HEAD
using System.IO;
using System.Net;
=======
<<<<<<< HEAD
using System.IO;
using System.Net;
=======
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
using System.Net.Http;
using System.Threading.Tasks;

namespace ForthCrs1._1.Dialogs
{
    [LuisModel("", "")]
    [Serializable]
    public class LuisDialog : LuisDialog<object>
    {
        const string uriBase = "https://api.cognitive.microsoft.com/bing/v7.0/images/search";
        //Initialize a new ForthcrsClient
        
        public static string imageweatherUrl;
        const string accessKey = "";
        public readonly BuildFormDelegate<DepartureDate> ReserveShip;
       
        [field: NonSerialized()]
        protected Activity _message;
        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }

        protected override async Task MessageReceived(IDialogContext context, IAwaitable<IMessageActivity> item)
        { 
             //Waiting user Message
            _message = (Activity)await item;

            await base.MessageReceived(context, item);

        }


        public LuisDialog(BuildFormDelegate<DepartureDate> reserveShip)
        {

            this.ReserveShip = reserveShip;

        }
        //We handle the message depending the user Input with LuisIntent

        //Case the input is not matching with a LuisIntent
        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult result)
        {

<<<<<<< HEAD
            await context.PostAsync("Sorry , I am not yet clever enough to understand you ! Can you be more accurate ?");
=======
<<<<<<< HEAD
            await context.PostAsync("Sorry , I am not yet clever enough to understand you ! Can you be more accurate ?");
=======
            await context.PostAsync("Sorry , I am not yet clever enough to understand you!Can you be more accurate ?");
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
            context.Wait(MessageReceived);

        }
        //Case the user is asking for the weather in certain city
        [LuisIntent("Weather")]
        public async Task Weather(IDialogContext context, LuisResult result)
        {

            var weather_city = result.Entities;
           
                string cityname = weather_city[0].Entity.ToString();
                string _address = "http://api.openweathermap.org/data/2.5/weather?q=" + cityname + "&APPID=";

                //Calling the weatherMap Api
                HttpClient client = new HttpClient();

            try
            {
                var response = await client.GetStringAsync(_address);

                JObject o = JObject.Parse(response);
                if (o["cod"].ToString() == "200")
                {

                    var bb = o["weather"][0]["description"];
                    string weather = bb.ToString();

                    //Returnig the weather in the certain city
                    await context.PostAsync("The weather in city " + UppercaseFirst(cityname) + " is " + weather);
                    //call Bing image search Api to get an image of the arrival location
                    string imageweather = cityname + " " + weather;
                    var uriQuery = uriBase + "?q=" + Uri.EscapeDataString(imageweather);
                    WebRequest request = HttpWebRequest.Create(uriQuery);
                    request.Headers["Ocp-Apim-Subscription-Key"] = accessKey;
                    HttpWebResponse response2 = (HttpWebResponse)request.GetResponseAsync().Result;
                    string json = new StreamReader(response2.GetResponseStream()).ReadToEnd();
                    var arrivalLocImage = JObject.Parse(json);
                    var root = arrivalLocImage["value"][0]["contentUrl"].ToString();
                    //Saving the image Url
                    imageweatherUrl = root;


                    var reply = context.MakeMessage();

                    reply.Attachments.Clear();

                    reply.Attachments.Add(new Attachment()

                    {

                        ContentUrl = imageweatherUrl,

                        ContentType = "image/jpeg",



                    });

                    context.PostAsync(reply);

                }

            }
            catch(Exception e)
            {
                await context.PostAsync("Sorry , I don't know the weather in the certain region right now . Would you like to help you with something else ?");

            }

         
            //We are waiting for the user  message
            context.Wait(MessageReceived);

        }
      
        //Case the user input is a rude word
        [LuisIntent("RudeWords")]
        public async Task RudeWords(IDialogContext context , LuisResult result)
        {

            string name = null;
            context.PrivateConversationData.TryGetValue<string>("FirstName", out name);
            await context.PostAsync("I am not so clever but i can understand that this is not so polite!!  \n "+ UppercaseFirst(name) +" don't talk like this to me,respect that i am only a bot!!How I can help you?");
            context.Wait(MessageReceived);

        }
        
        //Case the user is asking for the current time
        [LuisIntent("CurrentTime")]
        public async Task CurrentTime(IDialogContext context, LuisResult result)
        {
            string name = null;
            context.PrivateConversationData.TryGetValue<string>("FirstName", out name);
            string currentTime = DateTime.Now.ToString("h:mm");
            await context.PostAsync("It's "+currentTime +" o'clock !! Would you like to ask anything else "+ UppercaseFirst(name) +" ?");
            context.Wait(MessageReceived);

        }

        //Case the user  greets
        [LuisIntent("Greeting")]
        public async Task Greeting(IDialogContext context, LuisResult result)
        {  

            context.Call(new GreetingDialog(),Callback);

        }

        // Case the user After greets
        [LuisIntent("AfterGreeting")]
        public async Task AfterGreeting(IDialogContext context, LuisResult result)
        {

            context.Call(new AfterGreetingDialog(), Callback);

        }


        private async Task Callback(IDialogContext context, IAwaitable<object> result)
        {
           
            context.Wait(MessageReceived);

        }

        //Case the user asks for a trip Reservation
        [LuisIntent("ReserveShip")]
        public async Task ShipReservation(IDialogContext context, LuisResult result)
        {

           context.Call(new Traveldialog1(), Callback);
  
        }
        [LuisIntent("CancelReservation")]
        public async Task CancelReservation(IDialogContext context , LuisResult result)
        {

            context.Call(new CancelReservation(), Callback);

<<<<<<< HEAD
=======
<<<<<<< HEAD

        }
        [LuisIntent("Yes")]
        public async Task UserTypesYes(IDialogContext context,LuisResult result)
        {
            await context.PostAsync("What would you like to ask?");
            context.Wait(MessageReceived);
        }
        [LuisIntent("Help")]
        public async Task Help(IDialogContext context,LuisResult result)
        {
            string name = null;
            context.PrivateConversationData.TryGetValue<string>("FirstName", out name);
            List<string> helpOptions = new List<string>();
            helpOptions.Add("Reserve shipping tickets");
            helpOptions.Add("Cancel reservation");
            helpOptions.Add("Reset your conversation data");
            helpOptions.Add("Continue current conversation");
            PromptDialog.Choice(context, this.HelpOptions, helpOptions, "Ok, I will try to help you "+UppercaseFirst(name) +" , theese are your options : ",promptStyle:PromptStyle.Auto);
        }
        public async Task HelpOptions(IDialogContext context,IAwaitable<string> helpoption)
        {
            var helprequest = await helpoption;
            if (helprequest == "Reserve shipping tickets")
            {
                context.Call(new Traveldialog1(), Callback);
            }
            if (helprequest == "Cancel reservation")
            {
                context.Call(new CancelReservation(), Callback);
            }
            if (helprequest == "Reset your conversation data")
            {

=======
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2

        }
        [LuisIntent("Yes")]
        public async Task UserTypesYes(IDialogContext context,LuisResult result)
        {
            await context.PostAsync("What would you like to ask?");
            context.Wait(MessageReceived);
        }
        [LuisIntent("Help")]
        public async Task Help(IDialogContext context,LuisResult result)
        {
            string name = null;
            context.PrivateConversationData.TryGetValue<string>("FirstName", out name);
            List<string> helpOptions = new List<string>();
            helpOptions.Add("Reserve shipping tickets");
            helpOptions.Add("Cancel reservation");
            helpOptions.Add("Reset your conversation data");
            helpOptions.Add("Continue current conversation");
            PromptDialog.Choice(context, this.HelpOptions, helpOptions, "Ok i will try to help you "+UppercaseFirst(name) +" , theese are your options : ",promptStyle:PromptStyle.Auto);
        }
        public async Task HelpOptions(IDialogContext context,IAwaitable<string> helpoption)
        {
            var helprequest = await helpoption;
            if (helprequest == "Reserve shipping tickets")
            {
                context.Call(new Traveldialog1(), Callback);
            }
            if (helprequest == "Cancel reservation")
            {
                context.Call(new CancelReservation(), Callback);
            }
            if (helprequest == "Reset your conversation data")
            {

<<<<<<< HEAD
=======
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
              context.Call(new RootDialog(),Callback);

            }
            if (helprequest == "Continue current conversation")
            {
<<<<<<< HEAD
                await context.PostAsync("Ok , let's continue our conversation , ask me what you want !");
=======
<<<<<<< HEAD
                await context.PostAsync("Ok , let's continue our conversation , ask me what you want !");
=======
                await context.PostAsync("Ok ,let's continue our conversation,ask me what you want!");
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
                context.Wait(MessageReceived);
            }
        }
    }

}