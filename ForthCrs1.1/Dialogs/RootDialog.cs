﻿using ForthCrs1._1.Cards;
using ForthCrs1._1.OpenSea;
using ForthCrs1._1.ProfileForm;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Threading.Tasks;
using ForthCrs1._1.ApiCommunication;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using System.Collections.Generic;
using QuickType;
using System.Linq;

namespace ForthCrs1._1.Dialogs
{

    [Serializable]
    public class RootDialog : IDialog

    {


        private readonly BuildFormDelegate<DepartureDate> reserveship;

        //We receive the first message

        public async Task StartAsync(IDialogContext context)
        {    //We present the ForthCrsCard
            await context.PostAsync("Welcome to ForthCrsBot!!!");
            //  var card = Chain.From(() => new ForthCrsCard());
            var card = new ForthCrsCard();
            context.Call(card, MessageReceivedAsync);

        }
      
        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
              //We ask the users to complete his profile in ProfileForma
              var profileForm = Chain.From(() => FormDialog.FromForm(ProfileForma.BuildForm));
              context.Call<ProfileForma>(profileForm, Callback1);
            
        }
   

        private async Task Callback1(IDialogContext context, IAwaitable<ProfileForma> result)
        {

             var profileForm =await result;
             var FirstName = profileForm.FirstName;
            //We guide the user to LuisDialog so that we can handle his messages
             await context.PostAsync("How I can help you " + UppercaseFirst(FirstName) + "?");
             context.Call(new LuisDialog(reserveship), Callback2);
            

        }
      
        private async Task Callback2(IDialogContext context, IAwaitable<object> result)
        {

            context.Wait(MessageReceivedAsync);

        }
        public string UppercaseFirst(string s)
        {
              // Check for empty string.
              if (string.IsNullOrEmpty(s))
              {

                return string.Empty;
              }
              // Return char and concat substring.
              return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }

  
        

    }
}
