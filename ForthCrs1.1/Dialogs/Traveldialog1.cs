﻿using ForthCrs1._1.ApiCommunication;
using ForthCrs1._1.Cards;
using ForthCrs1._1.OpenSea;
using ForthCrs1._1.ProfileForm;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using QuickType;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mail;
using SendGrid;
using SendGrid.Helpers.Mail;
namespace ForthCrs1._1.Dialogs
{
   
    [Serializable]
    public class Traveldialog1 : IDialog<DepartureDate>
    {
        public readonly BuildFormDelegate<DepartureDate> ReserveShip;
        public readonly BuildFormDelegate<CaseNoRoots> ReserveShip2;

        public static List<TMsgPricingPassReq> AllPricePerPass = new List<TMsgPricingPassReq>();
        public static List<TMsgPricingPassReq> AllPassTable = AllPricePerPass;

        public static List<TClassAnalysisLocal> ClassAnalysisPerPass = new List<TClassAnalysisLocal>();
        public static List<TClassAnalysisLocal> ClassanalysisperPass = ClassAnalysisPerPass;

        public static List<TReserveIssuePerPassengerLocal> ReserveIssuePerPass = new List<TReserveIssuePerPassengerLocal>();
        public static List<TReserveIssuePerPassengerLocal> AllPassReserveIsuueTable = ReserveIssuePerPass;

        public static List<TMsgPricingVehicleReq> VehicleListStruct = new List<TMsgPricingVehicleReq>();
        public static List<TMsgPricingVehicleReq> AllVehTable = VehicleListStruct;


        public static List<TReserveIssuePerVehicleLocal> ReserveIssuePerVehicle = new List<TReserveIssuePerVehicleLocal>();
        public static List<TReserveIssuePerVehicleLocal> AllVehReserveIssueTable = ReserveIssuePerVehicle;

        public static List<int> min10DepartLocations = new List<int>();
        public static int completeAmount;
        public static string departureLoc= null;
        public static string arrivalLocationStat;
        public static int i = 1;
        public static int j = 1;
        
        //Starting the resrvation Dialog
        public async Task StartAsync(IDialogContext context)
        {

           //  List<From> from = Apicommunication.LocationCombination();
             await context.PostAsync(String.Format("Let me help you reserve tickets for your trip"));
            // List<string> DepaerturesLocations = from.Select(o => o.Name).ToList();
        //   PromptDialog.Choice<string>(context, this.SelectDeparture, from.Select(o => o.Name).ToArray(), "From where do you want to ship?", promptStyle: PromptStyle.Auto);
             PromptDialog.Text(context, this.DepartLoc, "From where do you want to ship ?");
        }
        private async Task DepartLoc(IDialogContext context, IAwaitable<string> result)

        {
            var departure = await result;
            string depart = departure.ToUpper();
            List<int> costs = new List<int>();
            List<From> from = Apicommunication.LocationCombination();
            List<string> DepaerturesLocations = from.Select(o => o.Name).ToList();
            foreach (string name in DepaerturesLocations)
            {

                int cost = LevenshensteinAlgorithm.CalcLevenshteinDistance(depart, name);
                costs.Add(cost);

            }

            int minimuc = costs.IndexOf(costs.Min());
            List<int> A = costs;

            var sorted = A
                .Select((x, i) => new KeyValuePair<int, int>(x, i))
                .OrderBy(x => x.Key)
                .ToList();

            List<int> B = sorted.Select(x => x.Key).ToList();
            List<int> idx = sorted.Select(x => x.Value).ToList();
            List<int> minc10 = idx.GetRange(1, 10);
            min10DepartLocations = minc10;
            
            string name3 = DepaerturesLocations[minimuc];
            await SelectDeparture(context, name3);
        }
        //User choose Departure Location
        private async Task SelectDeparture(IDialogContext context,string result)
        {

            string departureLoca =   result;
            departureLoc = departureLoca;
            PromptDialog.Confirm(context, this.ComfirmDepartureLoc, "Do you want to ship from " + departureLoc + " ?", promptStyle: PromptStyle.Auto);
        /*    //Saving Departure Location
            context.PrivateConversationData.SetValue<string>("DepartureLocation", departureLoc);
            List<From> from = Apicommunication.LocationCombination();
            string departureLocCode = from.Find(o => o.Name == departureLoc).Code;
            //Saving Departure Location Abbrevation
            context.PrivateConversationData.SetValue<string>("DepartureLocationCode", departureLocCode);
            var arrivalLocation = from.Find(o => o.Name ==departureLoc).To.ToElementArray.Select(o => o.Name);
            PromptDialog.Choice<string>(context, this.SelectArrival, arrivalLocation, "Where would you like to travel ?", promptStyle: PromptStyle.Auto);
            */
        }
        private async Task ComfirmDepartureLoc(IDialogContext context ,IAwaitable<bool> result)
        {
            var option = await result;
            if (option == true)
            {
                //Saving Departure Location
                context.PrivateConversationData.SetValue<string>("DepartureLocation", departureLoc);
                List<From> from = Apicommunication.LocationCombination();
                string departureLocCode = from.Find(o => o.Name == departureLoc).Code;
                //Saving Departure Location Abbrevation
                context.PrivateConversationData.SetValue<string>("DepartureLocationCode", departureLocCode);
                var arrivalLocation = from.Find(o => o.Name == departureLoc).To.ToElementArray.Select(o => o.Name);
                PromptDialog.Choice<string>(context, this.SelectArrival, arrivalLocation.Take(48), "Where would you like to travel ?", promptStyle: PromptStyle.Auto);
            }
            if (option == false)
            {
                List<string> Top10DepartLoc = new List<string>();
                List<From> from = Apicommunication.LocationCombination();
                List<string> DepaerturesLocations = from.Select(o => o.Name).ToList();
                for (int m = 0; m <= 9; m++)
                {
                    var departlocations = DepaerturesLocations[min10DepartLocations[m]];
                    Top10DepartLoc.Add(departlocations);
                }
                Top10DepartLoc.Add("NONE OF ABOVE");
                PromptDialog.Choice<string>(context, this.UserChangeDepartLoc, Top10DepartLoc,"Ok , choose your departure location from below :", promptStyle: PromptStyle.Auto);
            }
        }

        private async Task UserChangeDepartLoc(IDialogContext context,IAwaitable<string> result)
        {
            var departLocations = await result;
            List<From> from = Apicommunication.LocationCombination();
            if (departLocations == "NONE OF ABOVE")
            {

                await UserChooseFromWholeList(context);

            }
            else
            {
                context.PrivateConversationData.SetValue<string>("DepartureLocation", departLocations);
                departureLoc = departLocations;
                string departureLocCode = from.Find(o => o.Name == departLocations).Code;
                //Saving Departure Location Abbrevation
                context.PrivateConversationData.SetValue<string>("DepartureLocationCode", departureLocCode);
                var arrivalLocation = from.Find(o => o.Name == departureLoc).To.ToElementArray.Select(o => o.Name);
                PromptDialog.Choice<string>(context, this.SelectArrival, arrivalLocation.Take(42), "Where would you like to travel ?", promptStyle: PromptStyle.Auto);
            }
        }
      
        private async Task UserChooseFromWholeList(IDialogContext context)
        {
            List<From> from = Apicommunication.LocationCombination();
            List<string> DepaerturesLocations = from.Select(o => o.Name).ToList();
            PromptDialog.Choice<string>(context, this.DepartLoc, from.Select(o => o.Name).ToArray(), "Sorry,but you have to choose from the whole list :?", promptStyle: PromptStyle.Auto);
        }
        //User choose Arrival Location

        private async Task SelectArrival(IDialogContext context, IAwaitable<string> arrivalocation)
        {

            string arrivalLoc = await arrivalocation;
            //Saving Arrival Location
            context.PrivateConversationData.SetValue<string>("ArrivalLocation", arrivalLoc);
            arrivalLocationStat = arrivalLoc;
            List<From> from = Apicommunication.LocationCombination();
            string departureLocCode = null;
            context.PrivateConversationData.TryGetValue<string>("DepartureLocationCode", out departureLocCode);
            string arrivalLocCode= from.Find(o => o.Code == departureLocCode).To.ToElementArray.Find(a => a.Name == arrivalLoc).Code;
            //Saving Arrival Location Code Abbrevation
            context.PrivateConversationData.SetValue<string>("ArrivalLocationCode", arrivalLocCode);
            var travelforma = new FormDialog<DepartureDate>(new DepartureDate(), this.ReserveShip, FormOptions.PromptInStart);
            context.Call<DepartureDate>(travelforma, SelectDepatureDate);
            
        }
       
        //Getting the values fulfilled in DepartureDate Form
        private async Task SelectDepatureDate(IDialogContext context,IAwaitable<DepartureDate> result)
        {

            var travelform = await result;
            int travellersnumber = travelform.TravellersNumber;
            //Saving the number of passengers
            context.PrivateConversationData.SetValue<int>("TravellersNumber", travellersnumber);
            string departDate1 =travelform.Departuredate.ToString("MM/dd/yyyy");
            DateTime separ = DateTime.ParseExact(departDate1, "MM/dd/yyyy",CultureInfo.GetCultureInfoByIetfLanguageTag("el-GR"));
            string departureDate = separ.ToString("yyyyMMdd");
            //Saving the departure date  YYYYMMDD
            context.PrivateConversationData.SetValue<string>("DepartureDate", departureDate);
            await Callback1(context, departureDate);
            
        }

        //In case there are no roots the user tries another departure Date
        private async Task CaseNoRoots(IDialogContext context, IAwaitable<bool> result)
        {

            var option = await result;
            if (option == true)
            {

                //Calling the Casenoroots form
                var enrollmentForm = new FormDialog<CaseNoRoots>(new CaseNoRoots(), this.ReserveShip2, FormOptions.PromptInStart);
                //We call CaseNoRoots1 class so the user try another departure date
                context.Call<CaseNoRoots>(enrollmentForm, CaseNoRoots1);
            }
           else
            {
                //Returning to LuisDialog
                await  context.PostAsync("Do you want to ask anything else ?");
                context.Done("");

            }

        }

        //Saving the new departure date entered from the user
        private async Task CaseNoRoots1(IDialogContext context,IAwaitable<CaseNoRoots> result)
        {
            var travelform = await result;
            string timedepart = travelform.DepartureTime.ToString("MM/dd/yyyy");
            DateTime date = DateTime.ParseExact(timedepart, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            string departureDate = date.ToString("yyyyMMdd");
            //Saving the new departure Date  YYYYMMDD
            context.PrivateConversationData.SetValue<string>("DepartureDate", departureDate);
            await Callback1(context, departureDate);

        }

        //Checking if there are available ticket according to departure location ,arrival location,departure date
        private async Task Callback1(IDialogContext context,string result)
        {

            string arrivalLocCode = null;
            string departureLocCode = null;
            string departureDate = null;
            string departureLoc = null;
            string arrivalLoc = null; 
            context.PrivateConversationData.TryGetValue<string>("ArrivalLocationCode",out arrivalLocCode);
            context.PrivateConversationData.TryGetValue<string>("DepartureLocationCode", out departureLocCode);
            context.PrivateConversationData.TryGetValue<string>("DepartureDate", out departureDate);
            context.PrivateConversationData.TryGetValue<string>("DepartureLocation", out departureLoc);
            context.PrivateConversationData.TryGetValue<string>("ArrivalLocation", out arrivalLoc);
            THeaderInfo headers = Apicommunication.HeadersLoad();
            var a = Apicommunication.ClientProperties();
            a.ArrStation = arrivalLocCode;
            a.DepStation = departureLocCode;
            a.DepDate = departureDate;
            a.Sorting = "1";
            a.AvailInfo = "Y";
            a.DaysToSearch = null;
            a.Company = "TS2";
            a.Deptime = null;
            //We call the Api with the above properties anw we save the answer in ag list
            ItinerariesEx.ag = Apicommunication.GetItinerariesExt(a);
            var status =ItinerariesEx.ag.Select(o => o.Status).ToArray();
            var availability =ItinerariesEx. ag.Select(o => o.Available).ToArray();
            //If there are available tickets status = 000
            bool availabletickets = status.Contains("000");
            if (availabletickets == false)
            {

                     string res = departureDate;
                     DateTime d = DateTime.ParseExact(res, "yyyyMMdd", CultureInfo.InvariantCulture);
                     string timedepart = d.ToString("MM/dd/yyyy");
                     await context.PostAsync("There is no rout with available tickets from " + departureLoc + " to " + arrivalLoc + " on "+timedepart);
                     //User option if he want to try another departure date
                     PromptDialog.Confirm(context, CaseNoRoots, "Do you want to choose another departure date ?", promptStyle: PromptStyle.Auto);
             
            }
            else
            {

                    string res = departureDate;
                    DateTime d = DateTime.ParseExact(res, "yyyyMMdd", CultureInfo.InvariantCulture);
                    string timedepart = d.ToString("MM/dd/yyyy");
                    await context.PostAsync("There are tickets available from " +UppercaseFirst(departureLoc) + " to " +UppercaseFirst(arrivalLoc) + " at " + timedepart);
                    var SelectTime = ItinerariesEx.ag.FindAll(o => o.Status == "000").Select(o => o.Deptime).ToList();
                    //User choose departure Time
                    PromptDialog.Choice<string>(context, this.Callback2, SelectTime, "Please , choose depart time:", promptStyle: PromptStyle.Auto);

            }

        }

        //Getting the departure Time choosen from the user
        private async Task Callback2(IDialogContext context, IAwaitable<string> result)
        {

            var departureTime = await result;
            string departuretime = departureTime.Replace(@":", "");
            //We save the departure Time
            context.PrivateConversationData.SetValue<string>("DepartureTime",departuretime );
            string company = ItinerariesEx.ag.Find(O => O.Deptime == departureTime).Company;
            string str = company;
            str = str.Replace(" ", String.Empty);
            //We save the Company abbrevation for the certain departure Time
            context.PrivateConversationData.SetValue<string>("Company", str);
            string VesselID = ItinerariesEx.ag.Find(o => o.Company == company).VesselID;
            //We save the company vesselId
            context.PrivateConversationData.SetValue<string>("VesselId", VesselID);
            List<string> basicPrice = ItinerariesEx.ag.Find(o => o.Deptime == departureTime).ClassAvail.OrderByDescending(X=>X.ClassAdultBasicPrice).Select(K=>K.ClassAdultBasicPrice).ToList();
            List<string> ClassAbbr = ItinerariesEx.ag.Find(o => o.Deptime == departureTime).ClassAvail.Select(o => o.ClassAbbr).ToList();
            List<string> ClassType =ItinerariesEx.ag.Find(o => o.Deptime == departureTime).ClassAvail.Select(o => o.ClassPType).ToList();
            //We save the available class types and class abbrevations of the company
            context.PrivateConversationData.SetValue<List<string>>("ClassAbbrList", ClassAbbr);
            context.PrivateConversationData.SetValue<List<string>>("ClassTypeList", ClassType);     
            await CallBack3(context, company);

        }

        //Asking the user if he wants to change the number of all travellers
        private async Task CallBack3(IDialogContext context , string result)
        {

            int passengersNumber = new int();
            context.PrivateConversationData.TryGetValue<int>("TravellersNumber", out passengersNumber);
            await context.PostAsync("Let me continue with your reservation.There are " + passengersNumber.ToString() + " passengers");
            PromptDialog.Confirm(context, this.CallBack4, "Do want to book tickets for all the passengers?", promptStyle: PromptStyle.Auto);
            
         }

        //Getting the answer of the user
        private async Task CallBack4(IDialogContext context, IAwaitable<bool> result)
        {
         
            var conf = await result;
            int Travellersnumber = 0;
            if (conf == false)
            {
                //If answer = false we chain to ChangeNumberofTravellers Task
                PromptDialog.Text(context,ChangeNumberofTravellers,"Tell me the number of passengers you want to book a ticket.(1-9 passengers)" ,attempts:100);
          
            }
            else
            {
                //If answer = true we continue the reservation with the certain number of travellers
                context.PrivateConversationData.TryGetValue<int>("TravellersNumber", out Travellersnumber);
                await context.PostAsync("OK,let me help you to book tickets for " + Travellersnumber + " passenger(s) ");
                await TravellersNumber(context, Travellersnumber);

            }
            
        }

        //User option is to change the number of passengers
        private async Task ChangeNumberofTravellers(IDialogContext context, IAwaitable<string> result)
        {

            string passengerNumber = await result;
            passengerNumber = Regex.Match(passengerNumber, @"\d+").Value;
            int travellers = Int32.Parse(passengerNumber);
            if (travellers >= 1 && travellers <= 9)
            {

                context.PrivateConversationData.SetValue<int>("TravellersNumber", travellers);
                await TravellersNumber(context, travellers);
                
            }
            else
            {
                //Case the number of passengers entered is not valid
                PromptDialog.Text(context, ChangeNumberofTravellers, "The number of passengers must be minimum 1 and maximum 9.Please try again.", attempts: 100);
                         
            }
            
        }


        private async Task ChangeNumberofVehicles(IDialogContext context, IAwaitable<string> result)
        {


            string vehiclenumber = await result;

            vehiclenumber = Regex.Match(vehiclenumber, @"\d+").Value;
            int  vehicles = Int32.Parse(vehiclenumber);
            if (vehicles >= 1 && vehicles <= 4)
            {
                context.PrivateConversationData.SetValue<int>("VehiclesNumber", vehicles);
                await Callback14(context,vehicles);

            }
            else
            {
                PromptDialog.Text(context, ChangeNumberofVehicles, "The number of vehicles must be minimum 1 and maximum 4.Please try again.", attempts: 100);
                

            }

        }
      
        //We get the number of travallers.We chain to Callback5 so that we fulfill the options for all passengers
        private async Task TravellersNumber(IDialogContext context, int result)
        {
            
            int travellersnumber = result;
            TravellersNumberComplete.numberofTravellers = travellersnumber;
            context.PrivateConversationData.SetValue<int>("TravellersNumber", travellersnumber);
            if (i <= travellersnumber)
            {   

                await Callback5(context, travellersnumber);

            }
            else
            {

                //We finished with the fulfilled of all passengers
                i = 1;
                await Callback11(context, "");
            }
        }
        
        //We offer the user the option to choose a classtype for the certaion passenger
        private async Task Callback5(IDialogContext context ,int result)
        {
            
            int travellersnumber =  result;
            List<string> classType = null;
    //        TClassAnalysisLocal classanalysis = new TClassAnalysisLocal();
            context.PrivateConversationData.TryGetValue("ClassTypeList", out classType);
            context.PrivateConversationData.SetValue<int>("TravellersNumber", travellersnumber);
            List<string> ClassTypewithNog = classType.FindAll(O => O != "G").ToList();
            PromptDialog.Choice<string>(context,this.Callback6, ClassTypewithNog.Distinct(), "Please select the class  type for passenger number : " + i.ToString() +" (S - seat , C - cambine)", promptStyle: PromptStyle.Auto);
              
        }

        //We get the class type of the certain passenger,and the user must select the age range of the certain passenger
        private async Task Callback6(IDialogContext context,IAwaitable<string> result)
        {

            var classType = await result;
            //Saving ClasstYPE
            context.PrivateConversationData.SetValue<string>("ClassType", classType);
            List<string> ageRange = new List<string>();
            ageRange.Add("Adult");
            ageRange.Add("Child");
            ageRange.Add("Infant");
            ageRange.Add("Student");
            ageRange.Add("Senior");
            ageRange.Add("Youth");
            PromptDialog.Choice(context,this.Callback7,ageRange,"Select the age range of the current passenger",promptStyle: PromptStyle.Auto);

        }

        //We filter the passengers age range to get the PassType abbrevation
        public async Task Callback7(IDialogContext context , IAwaitable<string> result)
        {
            var passengerrange = await result;
            switch (passengerrange)
            {
                case "Adult":
                    context.PrivateConversationData.SetValue<string>("PassType", "AD");
                    break;
                case "Child":
                    context.PrivateConversationData.SetValue<string>("PassType", "CH");
                    break;
                case "Infant":
                    context.PrivateConversationData.SetValue<string>("PassType", "IN");
                    break;
                case "Student":
                    context.PrivateConversationData.SetValue<string>("PassType", "ST");
                    break;
                case "Senior":
                    context.PrivateConversationData.SetValue<string>("PassType", "SE");
                    break;
                case "Youth":
                    context.PrivateConversationData.SetValue<string>("PassType", "JU");
                    break;

            }
            //The user  choose the certain's passenger sex
            List<string> passSex = new List<string>();
            passSex.Add("Male");
            passSex.Add("Female");
            PromptDialog.Choice(context,this.Callback8,passSex,"Choose the gender of the current passenger",promptStyle: PromptStyle.Auto);


        }

        //We save the certain's passenger sex abbrevation and we ask his username
        private async Task Callback8(IDialogContext context , IAwaitable<string> result)
        {

            var passSex = await result;
            string passfirstletter = char.ToUpper(passSex[0]).ToString();
            //Saving passSex abbrevation
            context.PrivateConversationData.SetValue<string>("PassSex", passSex);
            PromptDialog.Text(context, Callback9, "Please type the last name of the current passenger",retry:null, attempts: 20);

        }

        //We save his username and we ask his first name
        private async Task Callback9 (IDialogContext context , IAwaitable<string> result)
        {

            var passname = await result;
            //Saving PassName
            context.PrivateConversationData.SetValue<string>("PassName", passname);
            PromptDialog.Text(context, Callback10, "Please type  passenger's first name",  attempts: 1);

        }

        //Getting his first name and saving the passenger properties and we fulfill the TMsgPricingPassReq,TClassAnalysisLocal,TReserveIssuePerPassengerLocal lists
        private async Task Callback10(IDialogContext context, IAwaitable<string> result)
        {
         
            var passinitial = await result;
            string passfirstletter = char.ToUpper(passinitial[0]).ToString();
            string passname = null;
            string passtype = null;
            string passsex = null;
            string classType = null;
            List<string> classabbr;
            List<string> classtype;
            int trav = 0;
            context.PrivateConversationData.TryGetValue<string>("PassName", out passname);
            context.PrivateConversationData.TryGetValue<string>("PassType", out passtype);
            context.PrivateConversationData.TryGetValue<string>("PassSex", out passsex);
            context.PrivateConversationData.TryGetValue<string>("ClassType", out classType);
            context.PrivateConversationData.TryGetValue<int>("TravellersNumber", out trav);
            context.PrivateConversationData.TryGetValue<List<string>>("ClassAbbrList", out classabbr);
            //Saving passenger's initial
            context.PrivateConversationData.SetValue<string>("PassInitial", passfirstletter);
            context.PrivateConversationData.TryGetValue<List<string>>("ClassTypeList", out classtype);
            string classAbbr = classabbr[classtype.IndexOf(classType)];
            //Saving passenger's classAbbr
            context.PrivateConversationData.SetValue<string>("ClassAbbr", classAbbr);
         
            TMsgPricingPassReq pricePerPassenger = new TMsgPricingPassReq() ;
            List<TMsgPricingPassReq> PpP = new List<TMsgPricingPassReq>();

            pricePerPassenger.ClassAbbr = classAbbr;
            pricePerPassenger.PassType = passtype;
            PpP.Add(pricePerPassenger);
            AllPricePerPass.AddRange(PpP);
            
            TClassAnalysisLocal Classanalysis = new TClassAnalysisLocal();
            List<TClassAnalysisLocal> CaL = new List<TClassAnalysisLocal>();
            Classanalysis.ClassAbbr = classAbbr;
            Classanalysis.Quantity = "1";
            Classanalysis.ClassResType = "W";
            CaL.Add(Classanalysis);
            ClassAnalysisPerPass.AddRange(CaL);
           

            TReserveIssuePerPassengerLocal ReserveIssuePerPass1 = new TReserveIssuePerPassengerLocal();
            List<TReserveIssuePerPassengerLocal> RiPp = new List<TReserveIssuePerPassengerLocal>();
            ReserveIssuePerPass1.ClassAbbr = classAbbr;
            ReserveIssuePerPass1.PassName = passname.ToUpper();
            ReserveIssuePerPass1.PassSex = passsex;
            ReserveIssuePerPass1.PassType = passtype;
            ReserveIssuePerPass1.PassInitial = passfirstletter;
            RiPp.Add(ReserveIssuePerPass1);
            ReserveIssuePerPass.AddRange(RiPp);
         
            i++;
            await TravellersNumber(context, trav);

        }
        //Check if the root offers vehicle reservation
        private async Task Callback11(IDialogContext context,string result)
        {
            List<string> classtype = null;
            context.PrivateConversationData.TryGetValue< List<string>>("ClassTypeList",out classtype);
            if (classtype.Contains("G"))
            {

                //If it offers we ask the user if he wants to reserve tickets for vehicles
                PromptDialog.Confirm(context, this.Callback12, "This rout offers vehicle reservation,you will have a vehicle? " , promptStyle: PromptStyle.Auto);

            }
            else
            {
                //If not we continue with Callback17
<<<<<<< HEAD
                await context.PostAsync("This rout is not offering reservation for vehicles");
=======
<<<<<<< HEAD
                await context.PostAsync("This rout is not offering reservation for vehicles");
=======
                await context.PostAsync("This root is not offering reservation for vehicles");
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
                await Callback17(context,"");
            }
        }

        //If user want's vehicle reservation we ask the number of vehicles,else we continue with Callback17
        private async Task Callback12(IDialogContext context ,IAwaitable<bool> result)
        {

            var option = await result;
            if (option)
            {

                PromptDialog.Text(context, this.Callback13, "Tell me the number of vehicles you will have(max number of vehicles :4)", attempts: 100);

            }
            else
            {
                int vehnumb = 0;
                context.PrivateConversationData.SetValue<int>("VehiclesNumber", vehnumb);
                await Callback17(context, "");

            }

        }

        //We get the number of vehicles
        private async Task Callback13(IDialogContext context,IAwaitable<string> result)
        {
            var vehicleNumber = await result;

            vehicleNumber = Regex.Match(vehicleNumber, @"\d+").Value;
            int vehiclesNumber = Int32.Parse(vehicleNumber);
            if (vehiclesNumber<=4)
            {
                //We continue with Callback14
                await Callback14(context, vehiclesNumber);

            }
            else
            {
                //If number of vehicles is invalid we chain to ChangeNumberofVehicles Task
                await ChangeNumberofVehicles(context, result);

            }
            
           
        }

        //Getting Vehicles number ,and offers the user to choosee the vehicle type for the certain vehicle
        private async Task Callback14(IDialogContext context, int result)
        {

            var vehiclenumber = result;
            //Saving number of vehicles
            context.PrivateConversationData.SetValue<int>("VehiclesNumber", vehiclenumber);
            //Calling Api to get vehicle list
            GetVehiclesTypeList.vl = Apicommunication.GetVehicleList();
            string company = null;
            context.PrivateConversationData.TryGetValue<string>("Company", out company);
            if (j<=vehiclenumber)
            {
                //Option for the user to choose the certain vehicle type and chain to Callback15
                var vehicleTypeOfCompany = GetVehiclesTypeList.vl.FindAll(o => o.Company == company).Select(k => k.Description).ToList();
                PromptDialog.Choice(context, this.Callback15, vehicleTypeOfCompany, "Choose vehicle type number:" + j, promptStyle: PromptStyle.Auto);

            }
            else
            {
                //if we fulfilled all vehicles properties we chain to Callback17
                j = 1;
                await Callback17(context, "");
            }
                
        }

        //For the certain vehicle type we save the vehicle VehicleTypeAbbr,VehicleMeters,VehicleDescription and we ask for plateNo 
        private async Task Callback15(IDialogContext context, IAwaitable<string> result)
        {

            var vehicletype = await result;
            context.PrivateConversationData.SetValue<string>("VehicleDescription", vehicletype);
            string vehicleTypeabbr = GetVehiclesTypeList.vl.Find(o => o.Description == vehicletype).VehicleTypeAbbr;
            string vehiclemeters = GetVehiclesTypeList.vl.Find(o => o.Description == vehicletype).Meters;
            context.PrivateConversationData.SetValue<string>("VehicleTypeAbbr", vehicleTypeabbr);
            context.PrivateConversationData.SetValue<string>("VehicleMeters", vehiclemeters);
            PromptDialog.Text(context,this.Callback16 ,"I would like to know the license plate number of the certain vehicle", attempts: 100);

        }

        //Getting the plateNo saving the certain vehicle properties to VehiclListStruct
        private async Task Callback16(IDialogContext context,IAwaitable<string> result)
        {

            var plateno = result;
            string vehicleAbrr = null;
            string vehiclemeters = null;
            string company = null;
            string description = null;
            string platenumb = plateno.ToString();
            string[] platenumb1 = platenumb.Split(' ');
            string platenumb3 = platenumb1[platenumb1.Length - 1];
            context.PrivateConversationData.SetValue<string>("PlateNo", platenumb3);
            context.PrivateConversationData.TryGetValue<string>("VehicleTypeAbbr", out vehicleAbrr);
            context.PrivateConversationData.TryGetValue<string>("VehicleMeters", out vehiclemeters);
            context.PrivateConversationData.TryGetValue<string>("Company", out company);
            context.PrivateConversationData.TryGetValue<string>("VehicleDescription", out description);

            TMsgPricingVehicleReq vehicleList = new TMsgPricingVehicleReq();
            List<TMsgPricingVehicleReq> TvLs = new List<TMsgPricingVehicleReq>();
            vehicleList.VehicleType = vehicleAbrr;
            vehicleList.Meters = vehiclemeters;
            TvLs.Add(vehicleList);
            VehicleListStruct.AddRange(TvLs);
            
           
            TReserveIssuePerVehicleLocal ReserveIssuePerVeh = new TReserveIssuePerVehicleLocal();
            List<TReserveIssuePerVehicleLocal> TrIpV = new List<TReserveIssuePerVehicleLocal>();
            ReserveIssuePerVeh.VehicleType = vehicleAbrr;
            ReserveIssuePerVeh.Meters = vehiclemeters;
            ReserveIssuePerVeh.PlateNo = platenumb3;
            TrIpV.Add(ReserveIssuePerVeh);
            ReserveIssuePerVehicle.AddRange(TrIpV);
     
            j++;
            int vehiclenumber = new int();
            context.PrivateConversationData.TryGetValue<int>("VehiclesNumber", out vehiclenumber);

            await Callback14(context, vehiclenumber);

        }

        //Calling the Api with AllPricePerPass,VehicleListStruct,ClassanalysisperPass to get the completePriceAnswer
        private async Task Callback17(IDialogContext context,string result)
        {

            var askPrice = Apicommunication.PriceRequest(AllPassTable,AllVehTable,ClassanalysisperPass,context);
            List<ItineraryPricingAnswer> completePriceAnswer = askPrice.ToList();
            var priceanswerCurrency = completePriceAnswer.Select(s => s.Currency).ToList();
            var priceanswerPerPass = completePriceAnswer.Select(s => s.PricesPerPassenger.ToList()).Select(O=>O.Select(K=>K.Price).ToList()).ToList();
            int vehNumb = 0;
            int CompleteAmountPveh = 0;
            List<int> pricespveh = null;
            context.PrivateConversationData.TryGetValue<int>("VehiclesNumber", out vehNumb);
            if (vehNumb != 0)
            {
                var priceanswerPerVehicle = completePriceAnswer.Select(s => s.PricesPerVehicle.ToList()).Select(O => O.Select(K => K.Price).ToList()).ToList();
                var pricePvehicle = priceanswerPerVehicle.SelectMany(x => x).ToList();
                pricespveh = pricePvehicle.Select(Int32.Parse).ToList();
                context.PrivateConversationData.SetValue<List<int>>("PricePerVehicle", pricespveh);
                CompleteAmountPveh = pricespveh.Aggregate((a, x) => a + x);
            }
            //dokimi
            var passengersName = AllPassReserveIsuueTable.Select(o => o.PassName).ToList();
            var vehdescription = AllVehReserveIssueTable.Select(l => l.VehicleType).ToList();
            context.PrivateConversationData.SetValue<List<string>>("VehicleType", vehdescription);
            context.PrivateConversationData.SetValue<List<string>>("PassengersName", passengersName);
            var passengersInitial = AllPassReserveIsuueTable.Select(o => o.PassInitial).ToList();
            context.PrivateConversationData.SetValue<List<string>>("PassengersInitial", passengersInitial);





            var extracharges = completePriceAnswer.Select(s => s.ExtraCharges).ToList();       
            var pricePpass = priceanswerPerPass.SelectMany(x =>x).ToList();         
            List<int> pricesppass = pricePpass.Select(Int32.Parse).ToList();      
            var CompleteAmountPpass = pricesppass.Aggregate((a, x) => a + x);       
            var completeamount = CompleteAmountPpass + CompleteAmountPveh + Int32.Parse(extracharges[0]);
            completeAmount = completeamount;
            int travelNumb = 0;
            int k = 1;
            int m = 1;
      
            context.PrivateConversationData.TryGetValue<int>("VehiclesNumber", out vehNumb);
            context.PrivateConversationData.TryGetValue<int>("TravellersNumber", out travelNumb);
            context.PrivateConversationData.SetValue<List<int>>("PricePerPass", pricesppass);
     
            context.PrivateConversationData.SetValue<List<string>>("ExtraCharges", extracharges);
            context.PrivateConversationData.SetValue<List<string>>("PriceCurrency", priceanswerCurrency);

<<<<<<< HEAD
            context.Call(new CarouselPricePrecentation(), ProcceedOrder);
         //   context.Call(new PricePresentationCard(), ProcceedOrder);
           
=======
<<<<<<< HEAD
            context.Call(new CarouselPricePrecentation(), ProcceedOrder);
         //   context.Call(new PricePresentationCard(), ProcceedOrder);
           
=======
         
            context.Call(new PricePresentationCard(), ProcceedOrder);
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
        }

        private async Task ProcceedOrder(IDialogContext context,IAwaitable<object> card)
        {
<<<<<<< HEAD

            PromptDialog.Confirm(context, this.Callback18, "Are you ok with this ? We will procced to purchase!! ", promptStyle: PromptStyle.Auto);

=======
<<<<<<< HEAD

            PromptDialog.Confirm(context, this.Callback18, "Are you ok with this ? We will procced to purchase!! ", promptStyle: PromptStyle.Auto);

=======
            PromptDialog.Confirm(context, this.Callback18, "Are you ok with this ? We will procced to purchase!! ", promptStyle: PromptStyle.Auto);
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
        }
        //Getting the user answer
        private async Task Callback18(IDialogContext context, IAwaitable<bool> result)
        {

            bool option = await result;
            if (option)
            {
                //If user agrees we chain the ReservationCard and continue with Callback19
              
                await Callback19(context);

            }
            else
            {
                //We return to the Parent dialog
                string name = null;
                completeAmount = 0;
                context.PrivateConversationData.TryGetValue("FirstName", out name);
                await context.PostAsync("Ok,maybe i can help you later to reserve your tickets\n");
<<<<<<< HEAD
                await  context.PostAsync("May i do something else for you " + UppercaseFirst(name) + " ?");
=======
<<<<<<< HEAD
                await  context.PostAsync("May i do something else for you " + UppercaseFirst(name) + " ?");
=======
                await  context.PostAsync("Can i do something else for you " + UppercaseFirst(name) + " ?");
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
                AllPassTable.Clear();
                AllPricePerPass.Clear();
                ReserveIssuePerPass.Clear();
                AllPassReserveIsuueTable.Clear();
                AllVehTable.Clear();
                AllVehReserveIssueTable.Clear();
                ClassanalysisperPass.Clear();
                ClassAnalysisPerPass.Clear();
                VehicleListStruct.Clear();
                ReserveIssuePerVehicle.Clear();
                completeAmount = 0;
                
               
                context.PrivateConversationData.SetValue<int>("TravellersNumber", 0);
                context.PrivateConversationData.SetValue<int>("VehiclesNumber", 0);
                context.Done("Ok,maybe one other time :)");

            }

        }

        private async Task Callback19(IDialogContext context)
        {
<<<<<<< HEAD

            var card = Chain.From(() => new ReservationCard());
=======
<<<<<<< HEAD

            var card = Chain.From(() => new ReservationCard());
=======
             var card = Chain.From(() => new ReservationCard());
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
            context.Call(card, this.Callback20);

        }
        private async Task Callback20(IDialogContext context,IAwaitable<object> card)
        {

            var card1 = await card;      
            var reserveAnswer = Apicommunication.Reservationanswer(ClassanalysisperPass, AllPassReserveIsuueTable, AllVehReserveIssueTable, context);
            var passengerTicket = reserveAnswer.Select(s => s.IssueAnswerPerPassenger.ToList()).Select(O => O.Select(K => K.TicketNo).ToList()).ToList();
            var passengersTicket = passengerTicket.SelectMany(x => x).ToList();
            var passengersName = AllPassReserveIsuueTable.Select(o => o.PassName).ToList();
            var vehdescription = AllVehReserveIssueTable.Select(k => k.VehicleType).ToList();
            context.PrivateConversationData.SetValue<List<string>>("VehicleType", vehdescription);
            context.PrivateConversationData.SetValue<List<string>>("PassengersName", passengersName);
            var passengersInitial = AllPassReserveIsuueTable.Select(o=> o.PassInitial).ToList();
            context.PrivateConversationData.SetValue<List<string>>("PassengersInitial", passengersInitial);
            context.PrivateConversationData.SetValue <List<string>>("PassengerTickets", passengersTicket);
            int vehNumb = 0;
            context.PrivateConversationData.TryGetValue<int>("VehiclesNumber", out vehNumb);
            if (vehNumb != 0)
            {
                var vehicleTicket = reserveAnswer.Select(s => s.IssueAnswerPerVehicle.ToList()).Select(O => O.Select(K => K.TicketNo).ToList()).ToList();
                var vehiclesTicket = vehicleTicket.SelectMany(x => x).ToList();
                context.PrivateConversationData.SetValue<List<string>>("VehiclesTickets", vehiclesTicket);
            }
            string FcrsReservationCode = reserveAnswer[0].CRSReservationCode;
            context.PrivateConversationData.SetValue<string>("CrsReservationCode", FcrsReservationCode);
            context.Wait(Callback21);

        }

        private async Task Callback21(IDialogContext context,IAwaitable<IMessageActivity> result)
        {

           var card = Chain.From(() => new ReceiptCardDialog());
           context.Call(card, this.Callback22);

        }
        private async Task Callback22(IDialogContext context,IAwaitable<object> card)
        {

            var a = await card;
            context.Wait(this.Callback23);

        }
        private async Task Callback23(IDialogContext context,IAwaitable<IMessageActivity> result)
        {
            PromptDialog.Text(context, this.GetUserEmail, "I would like to know your email!", attempts: 5);
        }
        private async Task GetUserEmail(IDialogContext context,IAwaitable<string> email)
        {

            var Useremail = await email;
            List<string> emailUser = ProfileForm.ExtractEmail.ExtractEmails(Useremail);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
             if (emailUser.Count != 0)
             {
                string useremail = emailUser[0];
                await SendUserEmail(context, useremail);

            }
              else
            {

                await WrongEmail(context, "");

            }

<<<<<<< HEAD
=======
=======
            if (emailUser.Count != 0)
            {
                string useremail = emailUser[0];
                await SendUserEmail(context, useremail);
            }
            else
            {
                await WrongEmail(context, "");
            }
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
        }
       private async Task WrongEmail(IDialogContext context,string result)
        {

              PromptDialog.Text(context, this.GetUserEmail, "This is not a valid email address.Please try another email!", attempts: 5);

        }

        private async Task SendUserEmail(IDialogContext context,string useremail)
        {

            string reservCode;
            string name;
            context.PrivateConversationData.TryGetValue<string>("CrsReservationCode", out reservCode);
            context.PrivateConversationData.TryGetValue<string>("FirstName", out name);

            try
            {

             

                 System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                 SmtpClient SmtpServer = new SmtpClient("smtp.sendgrid.net");

                 mail.From = new MailAddress("forthcrsbot@gmail.com");
                 mail.To.Add(useremail);
                 mail.Subject = "Reservation Code";
                 mail.Body ="Hi "+ UppercaseFirst(name) +",\n"+"This is your CrsReservationCode: "+reservCode +"\n"+"You can cancel you reservation by  typing 'cancel reservation' to our bot \n"+"Thank you for your purchase!";

                 SmtpServer.Port = 587;
                 SmtpServer.Credentials = new System.Net.NetworkCredential("apikey", "");
                 SmtpServer.EnableSsl = true;

                 SmtpServer.Send(mail);
                 await context.PostAsync("Mail Send , check your inbox!!");
                 await context.PostAsync("May I do something else for you?");

                
                completeAmount = 0;
                context.PrivateConversationData.SetValue<int>("TravellersNumber", 0);
                context.PrivateConversationData.SetValue<int>("VehiclesNumber", 0);
                AllPassTable.Clear();
                AllPricePerPass.Clear();
                ReserveIssuePerPass.Clear();
                AllPassReserveIsuueTable.Clear();
                AllVehTable.Clear();
                AllVehReserveIssueTable.Clear();
                ClassanalysisperPass.Clear();
                ClassAnalysisPerPass.Clear();
                VehicleListStruct.Clear();
                ReserveIssuePerVehicle.Clear();
                completeAmount = 0;
                context.Done("RESERVATIONFINISHED");

            }
            catch (Exception ex)
            {

                await WrongEmail(context, "");
              
            }
        }
<<<<<<< HEAD
                
=======
<<<<<<< HEAD
                
=======
        
        
>>>>>>> ad5f1ee82931f68119c4f62745a4934497b075c8
>>>>>>> 33dcda86c75a36ea4cfe2ba6c907c8a2206607d2
        public static class GetVehiclesTypeList
        {

            public static List<TVehiclesListStruct> vl { get; set; }

        }

        public static class ItinerariesEx
        {
            public static List<TTimetableAns1Ext> ag { get; set; }

        }
        public static class TravellersNumberComplete
        {

            public static int numberofTravellers { get; set; }

        }
        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {

                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }


    }


}