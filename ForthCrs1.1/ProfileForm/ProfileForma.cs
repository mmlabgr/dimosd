﻿using ForthCrs1._1.Dialogs;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Web;
using Microsoft.Bot.Connector;

namespace ForthCrs1._1.ProfileForm
{
    [Serializable]
  
    
    public class ProfileForma

    {   
        //they hold the data gathering from the form
        [Prompt("What is your first name? {||}")]
        public string FirstName;
        [Prompt("What is your last name? {||}")]
        public string LastName;
        [Prompt("What is your gender? {||}")]
        public Gender gender;

        public static IForm<ProfileForma> BuildForm()
        {
            return new FormBuilder<ProfileForma>()
                .Message("I see you are a new user. Let me help you fulfill your profile!!")
                .OnCompletion(async (context, ProfileForma) =>
                {//Set Bot UserData
                    context.PrivateConversationData.SetValue<bool>("ProfileComplete", true);
                    context.PrivateConversationData.SetValue<string>("FirstName", ProfileForma.FirstName);
                    context.PrivateConversationData.SetValue<string>("LastName", ProfileForma.LastName);
                    context.PrivateConversationData.SetValue<string>("Gender", ProfileForma.gender.ToString());
                    //tell the user tha the form is complete
                    await context.PostAsync("Your profile is complete!!");
                    

                })
                .Build();
        }

        [Serializable]
        public enum Gender
        {
            Male = 1, Female = 2
        };
  
    }
}